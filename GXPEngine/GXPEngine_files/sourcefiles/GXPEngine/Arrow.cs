﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;
public class Arrow : Sprite
{
    private Level _level;
    public float gravity;
    public bool isShot = false;
    public bool onWall = false;

    //Vectors
    private Vec2 _position;
    private Vec2 _velocity;


    public Arrow (Level pLevel, string pFilename) : base (pFilename)
    {
        SetOrigin(width/2, height/2);
        _velocity = new Vec2();
        _position = new Vec2(x, y);
        _level = pLevel;

    }

    //Bug proof public vectors
    public Vec2 position
    {
        set
        {
            _position = value ?? Vec2.zero;
        }
        get
        {
            return _position;
        }
    }

    public Vec2 velocity
    {
        set
        {
            _velocity = value ?? Vec2.zero;
        }
        get
        {
            return _velocity;
        }
    }

    private void wallCheck (List<Wall> walls)
    {
        foreach ( Wall wall in walls )
        {
            if ( HitTest(wall) )
            {
                //Sound wallhit = new Sound("sounds/soundeffects/wallhit/wall_hit_" + Utils.Random(1, 4) + ".wav", false);
                //wallhit.Play();
                _velocity.Scale(0);
                onWall = true;
            }
        }
    }

    public void Step ()
    {
        _position.Add(_velocity);
        x = _position.x;
        y = _position.y;
    }

    private void Update ()
    {
        if ( isShot && !onWall )
        {
            _velocity.Add(0, gravity);
            rotation = _velocity.Clone().GetAngleDegrees();
        }
        Step();
        wallCheck(_level.wallTiles);
    }
}
