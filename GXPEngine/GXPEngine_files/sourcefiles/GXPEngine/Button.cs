﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using GXPEngine;
using GXPEngine.Core;

public class Button : Sprite
{
    public bool isHighlighted
    {
        get; private set;
    }
    private Menu _menu;
    public string filename
    {
        get; private set;
    }
    public string activatedFilename
    {
        get; private set;
    }
    private Sprite _visualButton;
    public Button ( string pFilename, Menu pMenu, ParsedObject pObject ) : base(pFilename)
    {
        _menu = pMenu;

        this.filename = pFilename;
        activatedFilename = "menu/" + Path.GetFileNameWithoutExtension(this.filename) + "Activated.png";

        _visualButton = new Sprite(this.filename);
        _visualButton.SetOrigin(width / 2, height / 2);
        AddChild(_visualButton);

        SetOrigin(width / 2, height / 2);
        SetXY(pObject.X, pObject.Y);
        alpha = 0;
    }
    private void Update ()
    {
        if (HitTestPoint(Input.mouseX, Input.mouseY) )
        {
            isHighlighted = true;
        }
        else
        {
            isHighlighted = false;
        }

    }

    protected override void RenderSelf ( GLContext glContext )
    {
        if ( isHighlighted )
        {
            _visualButton.scale = 0.9f;
            _texture.SetBitmap(new System.Drawing.Bitmap(activatedFilename));
        }
        else
        {
            _visualButton.scale = 1f;
            _texture.SetBitmap(new System.Drawing.Bitmap(filename));

        }
        base.RenderSelf(glContext);
    }

    public bool isPressed ()
    {
        if ( Input.GetMouseButtonUp(0) && isHighlighted )
        {
            return true;
        }
        return false;
    }
    

}