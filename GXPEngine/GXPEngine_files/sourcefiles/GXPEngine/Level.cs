﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using GXPEngine;
using System.Threading.Tasks;

public class Level : GameObject
{

    public Map map
    {
        get; private set;
    }
    private TMXParser _tmxParser = new TMXParser();
    public int tileSize
    {
        get; private set;
    }
    private string _levelName;
    public List<Wall> wallTiles
    {
        get; private set;
    }

    private Player _player1;
    private Player _player2;
    public Player player1
    {
        get
        {
            return _player1;
        }
        set
        {
            player1 = _player1;
        }
    }
    public Player player2
    {
        get
        {
            return _player2;
        }
        set
        {
            player2 = _player2;
        }
    }


    public Player currentPlayer;
    //private Canvas _displayTileCount;
    public Canvas canvas;
    public Level (string pFilename)
    {
        _levelName = pFilename;
        map = _tmxParser.Parse(_levelName);


        tileSize = 64;

        if ( pFilename == "level.tmx" )
        {
            wallTiles = new List<Wall>();
            generateLevel();
            initializeObjects();


            x = game.width / 2 - (_player1.x + _player2.x) / 2;
        }


        //drawPolygons();
        //_displayTileCount = new Canvas(150, 20);
        //AddChild(_displayTileCount);
        //displayLevelName();


    }

    private void RespawnPlayer (Player pPlayer)
    {
        if ( map.ObjectGroup != null )
        {
            foreach ( ObjectGroup objectGroup in map.ObjectGroup )
            {
                if ( objectGroup.Object != null && pPlayer == _player1 )
                {
                    foreach ( ParsedObject _object in objectGroup.Object )
                    {
                        switch ( _object.Type )
                        {
                            case "player":
                                if ( _object.Name.ToLower() == "player1" )
                                {
                                    _player1 = new Player(this);
                                    setPlayerProperties(_object, _player1);
                                    _player1.name = "player1";
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    foreach ( ParsedObject _object in objectGroup.Object )
                    {
                        switch ( _object.Type )
                        {
                            case "player":
                                if ( _object.Name == "player2" )
                                {
                                    _player2 = new Player(this);
                                    setPlayerProperties(_object, _player2);
                                    _player2.name = "player2";
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    private void assignPlayerControlsAndAnimations (Player pPlayer)
    {
        if ( pPlayer == _player1 )
        {
            _player1.controls = new int[]
            {
            Key.A, Key.D, Key.W, Key.S, Key.LEFT_CTRL, Key.LEFT_SHIFT
            };
            pPlayer.animator = new PlayerAnimationsHandler("player1Animated.png", pPlayer);
        }
        else
        {
            _player2.controls = new int[]
            {
            Key.LEFT, Key.RIGHT, Key.UP, Key.DOWN, Key.RIGHT_CTRL, Key.RIGHT_SHIFT
            };
            pPlayer.animator = new PlayerAnimationsHandler("player2Animated.png", pPlayer);
        }
        pPlayer.AddChild(pPlayer.animator);
        pPlayer.animator.SetOrigin(pPlayer.width / 4, 0);


    }

    private void setPlayerProperties ( ParsedObject pObject, Player pPlayer )
    {
        AddChild(pPlayer);
        if ( pPlayer == _player1 )
        {
            pPlayer.SetXY(game.width - x - game.width + 200 ,pObject.Y);
        }
        else
        {
            pPlayer.SetXY(game.width - x - 200, pObject.Y);
        }
        pPlayer.position = new Vec2(pPlayer.x, pPlayer.y);
        assignPlayerProperties(pPlayer, pObject);
    }


    int time = 0;
    public bool RespawnPlayerTimer (Player pPlayer,ref bool pStartedCountdown)
    {

        if (pPlayer.currentState == Player.States.DEAD && pPlayer.animator.animationIsOver )
        {
            if ( pStartedCountdown )
            {
                time -= Time.deltaTime;
                Console.WriteLine(time);
            }
            else
            {
                time = pPlayer.respawnTime;
                pStartedCountdown = true;
            }

            if ( time <= 0 )
            {
                time = 0;

                RespawnPlayer(pPlayer);
                Console.WriteLine("Time is up!");
                pStartedCountdown = false;
                return true;
            }

        }
        return false;
    }


    // --------------------------- DEBUG -------------------------

    
    int scrollSpeed = 1;
    private void scrollCamera ()
    {
        if ( Input.GetKeyDown(Key.I) )
        {
            scrollSpeed += 5;
        }
        else if ( Input.GetKeyDown(Key.K) && scrollSpeed > 1 )
        {
            scrollSpeed -= 5;
        }
        else if ( Input.GetKeyDown(Key.O) )
        {
            scrollSpeed = 1;
        }
        if ( Input.GetKey(Key.J) )
        {
            x += scrollSpeed;
        }
        else if ( Input.GetKey(Key.L) )
        {
            x-= scrollSpeed;
        }
        //canvas.x = -x;

    }


    //------------------------ END DEBUG -------------------------


    //------------------------- UPDATE ---------------------------


    private void Update ()
    {
        Parallel.Invoke(InvokeRespawnPlayerTimers/*, InvokeGraphics*/, InvokeCheckArrowHits, InvokeCameraModifications, scrollCamera);
        WinCondition();
    }
    private Button victoryButton;
    private void WinCondition ()
    {
        foreach ( ObjectGroup pObjectGroup in map.ObjectGroup )
        {
            foreach ( ParsedObject pObject in pObjectGroup.Object )
            {
                if ( pObject.Name == "player1win" && Overlap(player1.x, player1.y, player1.width, player1.height, pObject.X, pObject.Y, pObject.X*3, pObject.Y*3) )
                {
                    victoryButton = new Button("victory1.png", null, pObject);
                    victoryButton.SetXY(game.width / 2 - x, game.height / 2 + y);
                    player1.Destroy();
                    player2.Destroy();
                    AddChild(victoryButton);

                }
                else if ( pObject.Name == "player2win" && Overlap(player1.x, player1.y, player1.width, player1.height, pObject.X, pObject.Y, pObject.X * 3, pObject.Y * 3) )
                {
                    victoryButton = new Button("victory2.png", null, pObject);
                    victoryButton.SetXY(game.width / 2 - x, game.height / 2 + y);
                    player1.Destroy();
                    player2.Destroy();
                    AddChild(victoryButton);

                }
            }
        }
        if ( victoryButton != null )
        {
            Console.WriteLine(victoryButton.x + " " + victoryButton.y);

        }
        if ( victoryButton != null && victoryButton.isPressed() && HitTestPoint(Input.mouseX, Input.mouseY) )
        {
            game.AddChild(new Menu(game as MyGame));
            Destroy();
        }
    }

    private void InvokeRespawnPlayerTimers ()
    {
        RespawnPlayerTimer(_player1, ref _player1.isRespawning);
        RespawnPlayerTimer(_player2, ref _player2.isRespawning);
    }

    public void InvokeCheckArrowHits ()
    {
        _player1.CheckArrowHit(_player2);
        _player2.CheckArrowHit(_player1);
    }

    private void InvokeCameraModifications ()
    {
        if ( _player1.currentState == Player.States.DEAD )
        {
            currentPlayer = _player2;
        }
        else if ( _player2.currentState == Player.States.DEAD )
        {
            currentPlayer = _player1;
        }
        cameraFollow(currentPlayer);
    }

    //private void InvokeGraphics ()
    //{
    //    if ( _player1 != null )
    //    {
    //        canvas.graphics.DrawString(_player1.currentState.ToString(), SystemFonts.CaptionFont, Brushes.Red, 100, 100);
    //    }
    //    if ( _player2 != null )
    //    {
    //        canvas.graphics.DrawString(_player2.currentState.ToString(), SystemFonts.CaptionFont, Brushes.Red, game.width - 100, 100);
    //    }
    //}
    //------------------------ END UPDATE ------------------------


    //TODO: MAKE THE CAMERA MOVE AFTER THE PLAYER CROSSES AN INVISIBLE BORDER SO IT'S NOT ALWAYS "GLUED" TO THE PLAYER
    private void cameraFollow (Player pTarget)
    {
        if ( pTarget != null )
        {
            int cameraXOffset = 440;

            if ( pTarget.x + x > game.width - cameraXOffset )
            {
                x -= pTarget.maxSpeed + 7;
            }
            else if ( pTarget.x + x < cameraXOffset )
            {
                x += pTarget.maxSpeed + 7;
            }

            //if ( canvas != null )
            //{
            //    canvas.x = -x;
            //}
        }
    }



    public bool TestCollision ( Sprite pOther )
    {
        int gridX = (int)(pOther.x / tileSize);
        int gridY = (int)(pOther.y / tileSize);
        int gridWidth = 2;
        int gridHeight = 3;

        for ( int layerNum = 0; layerNum < map.Layer.Length; layerNum++ )
        {
            Layer currentLayer = map.Layer[layerNum];
            if ( currentLayer.Name ==  "walls")
            {
                for ( int currentGridY = gridY; currentGridY < gridY + gridHeight; currentGridY++ )
                {
                    for ( int currentGridX = gridX; currentGridX < gridX + gridWidth; currentGridX++ )
                    {
                        int tile;
                        if ( (currentGridX > -1 && currentGridX < currentLayer.Width) && (currentGridY > -1 && currentGridY < currentLayer.Height ))
                        {
                            tile = currentLayer.GetLevelData()[currentGridY, currentGridX]; /*TODO: out-of-range*/
                        }
                        else
                        {
                            tile = 0;
                        }
                        if ( tile > 0 )
                        {
                            int wallX = currentGridX * tileSize;
                            int wallY = currentGridY * tileSize;
                            if ( Overlap(pOther.x, pOther.y, pOther.width, pOther.height, wallX + 1, wallY + 1, tileSize, tileSize) )
                            {
                                return true;
                            }
                        }               
                    }
                }
            }
        }
        return false;
    }

    private bool Overlap ( float pX1, float pY1, float pWidth1, float pHeight1, float pX2, float pY2, float pWidth2, float pHeight2 )
    {
        return Utils.RectsOverlap(pX1, pY1, pWidth1, pHeight1, pX2, pY2, pWidth2, pHeight2);
    }



    //----------------------- Generate level ---------------------



    /// <summary>
    /// Generates the tiles from tiled.
    /// </summary>
    private void generateLevel ()
    {
        Console.WriteLine("Generating level!");
        for ( int layerNum = 0; layerNum < map.Layer.Length; layerNum++ )
        {
            Layer currentLayer = map.Layer[layerNum];
            TileSet tileSet;
            int[,] levelData = currentLayer.GetLevelData();

            int LEVEL_WIDTH = map.Width;
            int LEVEL_HEIGHT = map.Height;
            for ( int row = 0; row < LEVEL_HEIGHT; row++ )
            {
                for ( int column = 0; column < LEVEL_WIDTH; column++ )
                {
                    int tile = levelData[row, column];
                    if ( tile > 0 )
                    {
                        for ( int i = 0; i < map.TileSet.Length; i++ )
                        {
                            if ( tile > map.TileSet[i].FirstGID - 1 && i == map.TileSet.Length - 1 )
                            {
                                tileSet = map.TileSet[map.TileSet.Length - 1];
                                setTileSize(tileSet);
                                createTile(column, row, tile, currentLayer, tileSet);
                            }
                            else if ( tile > map.TileSet[i].FirstGID - 1 && tile < map.TileSet[i + 1].FirstGID - 1 )
                            {
                                tileSet = map.TileSet[i];
                                setTileSize(tileSet);
                                createTile(column, row, tile, currentLayer, tileSet);
                            }
                        }
                    }
                }
            }
        }
    }

    private void setTileSize (TileSet pTileSet)
    {
        if ( pTileSet.TileHeight == pTileSet.TileWidth )
        {
            tileSize = pTileSet.TileHeight;
        }
        else if ( pTileSet.TileHeight > pTileSet.TileWidth )
        {
            tileSize = pTileSet.TileHeight;
        }
        else
        {
            tileSize = pTileSet.TileWidth;
        }
    }

    private void initializeObjects ()
    {
        Console.WriteLine("Initializing objects!");
        if ( map.ObjectGroup != null )
        {
            foreach ( ObjectGroup objectGroup in map.ObjectGroup )
            {
                if ( objectGroup.Object != null )
                {
                    foreach ( ParsedObject _object in objectGroup.Object )
                    {
                        switch ( _object.Type.ToLower() )
                        {
                            case "player":
                                if ( _object.Name.ToLower() == "player1" )
                                {
                                    _player1 = new Player(this);
                                    setPlayer(_object, _player1);
                                    _player1.name = "player1";
                                }
                                else
                                {
                                    _player2 = new Player(this);
                                    setPlayer(_object, _player2);
                                    _player2.name = "player2";
                                }
                                break;

                            default:
                                Console.WriteLine("No assigned objects!");
                                break;
                        }
                    }
                }
            }
        }
    }


    private void setPlayer(ParsedObject pObject,Player pPlayer)
    {
        AddChild(pPlayer);
        pPlayer.x = pObject.X;
        pPlayer.y = pObject.Y;
        pPlayer.position = new Vec2(pPlayer.x, pPlayer.y);
        assignPlayerProperties(pPlayer, pObject);
        
    }

    private void assignPlayerProperties (Player pPlayer, ParsedObject pObject)
    {
        assignPlayerControlsAndAnimations(pPlayer);

        for ( int i = 0; i < pObject.Properties.Property.Length; i++ ) //parses properties from tiled
        {
            switch ( pObject.Properties.Property[i].Name )
            {
                case "acceleration":
                    pPlayer.accelerationScale = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "friction":
                    pPlayer.friction = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "maxspeed":
                    pPlayer.maxSpeed = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "gravity":
                    pPlayer.gravity = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "jumpheight":
                    pPlayer.jumpHeight = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "chargespeed":
                    pPlayer.chargeSpeed = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "arrowspeed":
                    pPlayer.arrowSpeed = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "arrowgravity":
                    pPlayer.arrowGravity = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "respawntime":
                    pPlayer.respawnTime = int.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "maxarrowspeedmodifier":
                    pPlayer.maxArrowModifier = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                case "minarrowspeedmodifier":
                    pPlayer.minArrowModifier = float.Parse(pObject.Properties.Property[i].Value);
                    break;
                default:
                    break;
            }
        }
    }

    private void createTile (int pRow, int pColumn, int pTileID, Layer pLayer, TileSet pTileSet)
    {
        //Add a case statement to check different layers for tile types
        switch ( pLayer.Name )
        {
            case "walls":
                Wall wall = new Wall(pTileSet.Image.Source, pTileSet.Columns, pTileSet.Rows(tileSize), this);
                wall.currentFrame = pTileID - pTileSet.FirstGID;
                AddChild(wall);
                //sprite.SetOrigin(sprite.width /2, sprite.height / 2);
                wall.SetXY(pRow * map.TileHeight, pColumn * map.TileWidth);
                if ( wallTiles != null )
                {
                    wallTiles.Add(wall);

                }
                break;
            default:
                //Non object tiles - background
                AnimationSprite sprite = new AnimationSprite(pTileSet.Image.Source, pTileSet.Columns, pTileSet.Rows(tileSize));
                sprite.currentFrame = pTileID - pTileSet.FirstGID;
                AddChild(sprite);
                //sprite.SetOrigin(sprite.width /2, sprite.height / 2);
                sprite.SetXY(pRow * tileSize, pColumn * tileSize);
                break;
        }
    }


    //-------------------- END Generate level --------------------


    //private void drawPolygons ()
    //{
    //    if ( map.ObjectGroup != null )
    //    {
    //        foreach ( ObjectGroup objectGroup in map.ObjectGroup )
    //        {
    //            foreach ( ParsedObject _object in objectGroup.Object )
    //            {
    //                Canvas polygonCanvas = new Canvas(map.Width * _tileSize, map.Height * _tileSize);
    //                polygonCanvas.graphics.TranslateTransform(_object.X, _object.Y);
    //                AddChild(polygonCanvas);

    //                Polygon polygon = _object.Polygon;
    //                polygonCanvas.graphics.Clear(Color.Empty);
    //                polygonCanvas.graphics.DrawPolygon(Pens.AliceBlue, polygon.GetPoints());
    //            }
    //        }
    //    }
    //}
}