﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;

public class Menu : Level
{
    private Button _startGameButton;
    private Button _settingsButton;
    private Button _exitButton;
    private MyGame _myGame;
    SoundChannel menuMusic;
    
    public Menu (MyGame pMyGame) : base("menu.tmx")
    {
        AddChild(new Sprite("title.png"));
        menuMusic = new SoundChannel(0);
        Sound drums = new Sound("sounds/Drums.mp3", true);
        //drums.Play();
        _myGame = pMyGame;
        foreach ( ParsedObject currentObject in map.ObjectGroup[0].Object )
        {
            switch ( currentObject.Name )
            {
                case "start":
                    AddButton(ref _startGameButton, "start.png", currentObject);
                    break;
                case "settings":
                    AddButton(ref _settingsButton, "settings.png", currentObject);
                    break;
                case "exit":
                    AddButton(ref _exitButton, "exit.png", currentObject);
                    break;
                case "title":
                    Sprite sprite = new Sprite("menu/title.png");
                    AddChild(sprite);
                    sprite.SetOrigin(sprite.width/2, sprite.height/2);
                    sprite.scale = 0.7f;
                    sprite.x = currentObject.X;
                    sprite.y = currentObject.Y;
                    break;
                default:
                    break;
            }
        }


    }
    private void Update ()
    {
        //_startGameButton.Destroy();
        //_startGameButton = new Button(_startGameButton.name, this);
        //AddChild(_startGameButton);
        if ( _startGameButton != null && _startGameButton.isPressed() )
        {
            _myGame.level = new Level("level.tmx");
            _myGame.AddChild(_myGame.level);    
            Destroy();
        }
        else if ( _settingsButton != null && _settingsButton.isPressed() )
        {

        }
        else if ( _exitButton != null && _exitButton.isPressed())
        {
            game.Destroy();
        }

    }

    private void AddButton (ref Button pButton, string pFilename, ParsedObject pObject)
    {

        pButton = new Button("menu/" + pFilename, this, pObject);
        AddChild(pButton);
    }
}