using System;
using System.Drawing;
using GXPEngine.OpenGL;
using GXPEngine;

public class MyGame : Game //MyGame is a Game
{
    public Level level;
    //initialize game here
    public MyGame (int screenWidth, int screenHeight, bool fullscreen, bool vSync) : base(screenWidth, screenHeight, fullscreen, vSync)
	{
        AddChild(new Menu(this));
        
	}

	//update game here
	void Update ()
	{
        //if ( level != null )
        //{
        //    level.canvas.graphics.Clear(Color.LightGray);
        //    level.canvas.graphics.DrawString(currentFps.ToString(), SystemFonts.MenuFont, Brushes.Black, game.width / 2 - 50, 200);
        //}

    }



    //system starts here
    static void Main() 
	{
        TMXParser tmx = new TMXParser();
        tmx.Parse("level.tmx");
        bool fullscreen = false;
        bool vSync = true;
        int screenWidth = 0;
        int screenHeight = 0;
        for ( int i = 0; i < tmx.Map.Properties.Property.Length; i++ )
        {
            switch ( tmx.Map.Properties.Property[i].Name.ToLower() )
            {
                case "fullscreen":
                    fullscreen = bool.Parse( tmx.Map.Properties.Property[i].Value);
                    break;
                case "vsync":
                    vSync = bool.Parse(tmx.Map.Properties.Property[i].Value);
                    break;
                case "screenwidth":
                    screenWidth = int.Parse(tmx.Map.Properties.Property[i].Value);
                    break;
                case "screenheight":
                    screenHeight = int.Parse(tmx.Map.Properties.Property[i].Value);
                    break;
                default:
                    break;
            }
        }

		new MyGame(screenWidth, screenHeight, false, vSync).Start();
	}
}
