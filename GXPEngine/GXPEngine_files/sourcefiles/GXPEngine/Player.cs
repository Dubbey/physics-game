﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;
using System.Threading.Tasks;
public class Player : Sprite
{
    //State machines
    public enum States
    {
        IDLE,
        AIR,
        WALKING,
        SLIDING,
        SHOOTING_FORWARD,
        SHOOTING_FORWARDUP,
        SHOOTING_FORWARDDOWN,
        SHOOTING_UP,
        AIR_SHOOTING_FORWARD,
        AIR_SHOOTING_FORWARDUP,
        AIR_SHOOTING_FORWARDDOWN,
        AIR_SHOOTING_UP,
        DEAD
    }
    private enum Direction
    {
        LEFT,
        RIGHT,
        NONE
    }

    public States currentState;

    public void ResetState ()
    {
        currentState = States.IDLE;
    }

    private Level _level;

    //Vectors
    private Vec2 _position;
    private Vec2 _velocity;

    //Booleans
    private bool _chargingArrow = false;
    public bool isRespawning = false;

    //Tiled properties - Yes, I know I could've made them as a dictionary but I thought of it too late.
    public float accelerationScale;
    public float friction;
    public float gravity;
    public float maxSpeed;
    public float jumpHeight;
    public int respawnTime;
    public float maxArrowModifier;
    public float minArrowModifier;

    //Arrow related
    private Arrow _arrow;
    public float chargeSpeed;
    public float arrowSpeed;
    public float arrowGravity;
    private int _lastArrowRotation;
    private List<Arrow> _arrows = new List<Arrow>();

    //Controls
    public int[] controls;

    //Animations
    private PlayerAnimationsHandler _animator;
    public PlayerAnimationsHandler animator
    {
        get
        {
            return _animator;
        }
        set
        {
            _animator = value;
        }
    }

    public Player (Level pLevel): base("player.png")
    {
        _level = pLevel;
        _velocity = new Vec2();
        alpha = 0;
    }

    //Bug proof public vectors
    public Vec2 position
    {
        set
        {
            _position = value ?? Vec2.zero;
        }
        get
        {
            return _position;
        }
    }

    public Vec2 velocity
    {
        set
        {
            _velocity = value ?? Vec2.zero;
        }
        get
        {
            return _velocity;
        }
    }

    private void HorizontalMovement ()
    {
        Direction currentDirection = Direction.NONE;
        if ( !_chargingArrow )
        {
            //LEFT movement
            if ( Input.GetKey(controls[0]) )
            {
                currentDirection = Direction.LEFT;
                animator.Mirror(true, false);
                _velocity.x -= accelerationScale;
                if ( _velocity.x <= -maxSpeed )
                {
                    _velocity.x += accelerationScale;
                }
            }
            //RIGHT movement
            if ( Input.GetKey(controls[1]) )
            {
                currentDirection = Direction.RIGHT;
                animator.Mirror(false, false);
                _velocity.x += accelerationScale;
                if ( _velocity.x >= maxSpeed )
                {
                    _velocity.x -= accelerationScale;
                }
            }
            CanMove(_velocity.x, 0);
        }

        if ( !CanMove(_velocity.x, 0) )
        {
            _velocity.x = 0;
        }
        //Sliding from friction
        bool movingRight = _velocity.x > 0;
        bool movingLeft = _velocity.x < 0;

        if ( currentState != States.DEAD && !_chargingArrow)
        {

            if ( (movingLeft && currentDirection == Direction.RIGHT) || (movingRight && currentDirection == Direction.LEFT) )
            {
                //if ( !_chargingArrow )
                //{
                //    animator.SetState(States.SLIDING, false);
                //}
                _velocity.x *= friction;
            }
            else if ( (movingRight && currentDirection == Direction.RIGHT) || (movingLeft && currentDirection == Direction.LEFT) /*&& (currentState == States.IDLE || currentState == States.SLIDING)*/ )
            {
                animator.SetState(States.WALKING, false);
                Console.WriteLine("walking");
            }
            else
            {
                if ( _velocity.y == 0 && !_chargingArrow && (currentState != States.AIR && currentState != States.SLIDING) )
                {
                    Console.WriteLine("idle");
                    animator.SetState(States.IDLE, false);
                }
                _velocity.x *= friction;
            }
        }
        
    }

    /// <summary>
    /// Handles the movement.
    /// </summary>
    private void VerticalMovement ()
    {
        _velocity.y += gravity;

        if ( _velocity.y != 0 && currentState == States.IDLE )
        {
            animator.SetState(States.AIR, false);
        }
        //Gravity
        if ( !CanMove(0, _velocity.y) && currentState != States.DEAD )
        {
            //Jump
            if ( Input.GetKey(controls[4]) && !_chargingArrow )
            {
                _velocity.y = -jumpHeight;
                animator.SetState(States.AIR, false);
            }
            else
            {
                //currentState = States.IDLE;
                _velocity.y = 0;
                if ( currentState != States.WALKING && currentState != States.SLIDING )
                {
                    animator.SetState(States.IDLE, false);

                }
            }

        }
    }

    float arrowModifier = 0;
    int arrowDelay = 0;

    /// <summary>
    /// Handles arrow shooting.
    /// </summary>
    private void ShootArrow ()
    {
        if ( Input.GetKeyDown(controls[5]) )
        {
            animator.SetState(States.SHOOTING_FORWARD,true);
            if ( _arrows.Count != 0 )
            {
                if ( _arrows.Count > 3 )
                {
                    _arrows[0].Destroy();
                    _arrows.RemoveAt(0);
                }
            }

            _arrow = new Arrow(_level, "Arrow.png");
            if ( this.name == "player2" )
            {
                _arrow.color = 0x00bbff;
            }
            _arrows.Add(_arrow);
            _arrow.position.SetXY(x + width / 2, y + height / 2);
            _level.AddChild(_arrow);
            _arrow.gravity = arrowGravity;
            arrowModifier = minArrowModifier;
            _chargingArrow = true;
            if ( arrowDelay <= 0 )
            {
                arrowDelay = 2000;
            }
        }

        if ( _chargingArrow )
        {
            AimArrow();

            if ( arrowModifier < maxArrowModifier )
            {
                arrowModifier += Time.deltaTime;
            }
            else
            {
                arrowModifier = maxArrowModifier;
            }
            _arrow.position.SetXY(x + width / 2, y + height/2 - 11);

        }

        if ( Input.GetKeyUp(controls[5]) )
        {
            //if ( !CanMove(0, _velocity.y) )
            //{
            //    animator.SetState(States.SHOOTING_FORWARD, true);

            //}
            //else
            //{
            //    animator.SetState(States.AIR_SHOOTING_FORWARD, true);
            //}

            _arrow.velocity.Add(arrowSpeed * (arrowModifier / 1000), 0);
            _arrow.velocity.SetAngleDegrees(_arrow.rotation);
            _arrow.isShot = true;
            _chargingArrow = false;
        }



    }
    public bool CheckArrowHit ( Player other )
    {
        if ( _arrow != null && _arrow.HitTest(other) && _arrow.isShot && !_arrow.onWall )
        {
            Sound hitsound = new Sound("sounds/soundeffects/playerhit/player_hit_" + Utils.Random(1,4) + ".wav", false);
            hitsound.Play();
            other.animator.SetState(States.DEAD, true);
            _arrow.Destroy();
            if ( other.animator.animationIsOver )
            {
                return true;
            }
        }
        return false;
    }

    private States _lastShootingState;

    private void AimArrow ()
    {
        bool airShoot = false;
        if ( _velocity.y != 0 )
        {
            airShoot = true;
        }
        if ( Input.GetKey(controls[0]) && Input.GetKey(controls[2]) )
        {
            _lastArrowRotation = 225;
            animator.Mirror(true, false);
            if ( !airShoot )
            {
                _lastShootingState = States.SHOOTING_FORWARDUP;
            }
            else
            {
                _lastShootingState = States.AIR_SHOOTING_FORWARDUP;
            }
        }
        else if ( Input.GetKey(controls[1]) && Input.GetKey(controls[2]) )
        {
            _lastArrowRotation = 315;
            animator.Mirror(false, false);
            if ( !airShoot )
            {
                _lastShootingState = States.SHOOTING_FORWARDUP;
            }
            else
            {
                _lastShootingState = States.AIR_SHOOTING_FORWARDUP;
            }
        }
        else if ( Input.GetKey(controls[0]) && Input.GetKey(controls[3]) )
        {
            _lastArrowRotation = 135;
            animator.Mirror(true, false);
            if ( !airShoot )
            {
                _lastShootingState = States.SHOOTING_FORWARDDOWN;
            }
            else
            {
                _lastShootingState = States.AIR_SHOOTING_FORWARDDOWN;
            }

        }
        else if ( Input.GetKey(controls[1]) && Input.GetKey(controls[3]) )
        {
            _lastArrowRotation = 45;
            animator.Mirror(false, false);
            if ( !airShoot )
            {
                _lastShootingState = States.SHOOTING_FORWARDDOWN;
            }
            else
            {
                _lastShootingState = States.AIR_SHOOTING_FORWARDDOWN;
            }
        }
        else if ( Input.GetKey(controls[0]) )
        {
            _lastArrowRotation = 180;
            animator.Mirror(true, false);
            if ( !airShoot )
            {
                _lastShootingState = States.SHOOTING_FORWARD;
            }
            else
            {
                _lastShootingState = States.AIR_SHOOTING_FORWARD;
            }
        }
        else if ( Input.GetKey(controls[1]) )
        {
            _lastArrowRotation = 0;
            animator.Mirror(false, false);
            if ( !airShoot )
            {
                _lastShootingState = States.SHOOTING_FORWARD;
            }
            else
            {
                _lastShootingState = States.AIR_SHOOTING_FORWARD;
            }
        }
        else if ( Input.GetKey(controls[2]) )
        {
            _lastArrowRotation = -90;
            if ( !airShoot )
            {
                _lastShootingState = States.SHOOTING_UP;
            }
            else
            {
                _lastShootingState = States.AIR_SHOOTING_UP;
            }
        }
        if ( _lastShootingState == States.IDLE || _lastShootingState == States.WALKING )
        {
            _lastShootingState = States.SHOOTING_FORWARD;
        }
        else if (_lastShootingState == States.AIR)
        {
            _lastShootingState = States.AIR_SHOOTING_FORWARD;
        }
        animator.SetState(_lastShootingState, true);
        _arrow.rotation = _lastArrowRotation;
    }
    public void CheckBorderDeath ()
    {
        if ( y > game.height )
        {
            animator.SetState(States.DEAD, false);
            Destroy();
            //return true;
        }
        //return false;
    }

    private bool CanMove ( float pMoveX, float pMoveY )
    {
        bool result = false;
        //Idle state if the speed is really low but not exactly 0
        if ( _velocity.Length() < 0.01 && currentState != States.DEAD )
        {
            _velocity.Scale(0);
        }


        int length = (int)Mathf.Sqrt(pMoveX * pMoveX + pMoveY * pMoveY);
        if ( length < 1 )
            return true;

        pMoveX /= length;
        pMoveY /= length;
        Parallel.For(0, length, (ParallelOptions,state) =>
      {
          if ( !MoveAndTestCollisionStep(pMoveX, pMoveY) )
          {
              result = false;
              state.Stop();
          }
          else
          {
              result = true;
          }
      });

        return result;
    }

    private bool MoveAndTestCollisionStep ( float pMoveX, float pMoveY )
    {
        //move to new position
        x = x + pMoveX;
        y = y + pMoveY;

        _position.SetXY(x, y);

        //do a hittest
        if ( _level.TestCollision(this) )
        {
            //undo movement
            x = x - pMoveX;
            y = y - pMoveY;
        
            _position.SetXY(x, y);
            //no success!
            return false;
        }
        //it worked!
        return true;
    }

    private void Update ()
    {
        if ( currentState != States.DEAD )
        {
            HorizontalMovement();
            VerticalMovement();
            CheckBorderDeath();
            ShootArrow();
        }
    }

    protected override void OnDestroy ()
    {
        foreach ( Arrow arrow in _arrows )
        {
            if ( arrow.onWall )
            {
                arrow.Destroy();
            }
        }
        _arrows.Clear();
        base.OnDestroy();
    }
}
