﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;
public class PlayerAnimationsHandler : AnimationSprite
{

    private Dictionary<Player.States, int[]> _sequence = new Dictionary<Player.States, int[]> {
        { Player.States.IDLE, new int[10]{ 0,1,2,3,4,5,6,7,8,9 } },
        { Player.States.AIR, new int[1]{ 10 } },
        { Player.States.WALKING, new int[8]{ 11,12,13,14,15,16,17,18 } },
        { Player.States.SLIDING, new int[1]{ 19 } },
        { Player.States.SHOOTING_FORWARD, new int[6]{ 24,25,20,21,22,23 } },
        { Player.States.SHOOTING_FORWARDUP, new int[6]{ 30,31,26,27,28,29 } },
        { Player.States.SHOOTING_FORWARDDOWN, new int[6]{ 36,37,32,33,34,35 } },
        { Player.States.SHOOTING_UP, new int[6]{ 42,43,38,39,40,41 } },
        { Player.States.AIR_SHOOTING_FORWARD, new int[6]{ 48,49,44,45,46,47 } },
        { Player.States.AIR_SHOOTING_FORWARDUP, new int[6]{ 54,55,50,51,52,53 } },
        { Player.States.AIR_SHOOTING_FORWARDDOWN, new int[6]{ 60,61,56,57,58,59 } },
        { Player.States.AIR_SHOOTING_UP, new int[6]{ 66,67,62,63,64,65 } },
        { Player.States.DEAD, new int[3]{ 68,69,70 } },
    };


    private const int _frametime = 0;
    private int[] _currentAnimation;
    private Player _player;
    private float _animationSpeed;
    private bool _animationIsOver = false;
    private bool _shootingAnimation = false;
    public bool animationIsOver
    {
        get
        {
            return _animationIsOver;
        }
        set
        {
            animationIsOver = _animationIsOver;
        }
    }
    private MyGame _myGame;
    public PlayerAnimationsHandler (string pFilename, Player pPlayer) : base(pFilename, 7, 11)
    {
        _myGame = game as MyGame;
        _player = pPlayer;
    }

    public void SetState (Player.States pNewState, bool shootingAnimation)
    {
        _shootingAnimation = shootingAnimation;
        if ( _player.currentState != pNewState )
        {
            _currentAnimation = _sequence[pNewState];
            _player.currentState = pNewState;
            _animationSpeed = 1f;
            //if ( pNewState == Player.States.WALKING )
            //{
            //    _animationSpeed = 1f;
            //}
            //else
            //{
            //    _frametime = 0;
            //}
        }
    }

    private void Animate ()
    {
        if ( _currentAnimation != null && _currentAnimation.Length > 0 )
        {
            if ( _player.currentState == Player.States.WALKING )
            {
                _animationSpeed = 1f;
            }
            int frame = (int)(Mathf.Floor(Time.now/ 100) * _animationSpeed ) % _currentAnimation.Length;
            if ( !_shootingAnimation && frame >= 0 )
            {
                currentFrame = _currentAnimation[frame];
            }
            else if ( _shootingAnimation && !_animationIsOver )
            {
                int shootingFrame = (int)Mathf.Floor(Time.now /500 * _animationSpeed ) % _currentAnimation.Length;
                currentFrame = _currentAnimation[shootingFrame];
            }

            _animationIsOver = false;
            if ( currentFrame == _currentAnimation[_currentAnimation.Length-1] )
            {
                _animationIsOver = true;
            }
        }

    }

    private void Update ()
    {
        Animate();
    }
}
