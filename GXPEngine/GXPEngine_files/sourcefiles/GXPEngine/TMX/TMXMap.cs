﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using GXPEngine;

[XmlRoot("map")]
public partial class Map
{
    [XmlAttribute("version")]
    public string Version = "";

    [XmlAttribute("orientation")]
    public string Orientation = "";

    [XmlAttribute("renderorder")]
    public string RenderOrder = "";

    [XmlAttribute("width")]
    public int Width = 0;

    [XmlAttribute("height")]
    public int Height = 0;

    [XmlAttribute("tilewidth")]
    public int TileWidth = 0;

    [XmlAttribute("tileheight")]
    public int TileHeight = 0;

    [XmlAttribute("nextobjectid")]
    public int NextObjectID = 0;

    [XmlElement("properties")]
    public Properties Properties;

    [XmlElement("tileset")]
    public TileSet[] TileSet;

    [XmlElement("layer")]
    public Layer[] Layer;

    [XmlElement("objectgroup")]
    public ObjectGroup[] ObjectGroup;

    public Map ()
    {
    }
}

[XmlRoot("tileset")]
public partial class TileSet
{
    [XmlAttribute("firstgid")]
    public int FirstGID = 0;

    [XmlAttribute("name")]
    public string Name = "";

    [XmlAttribute("tilewidth")]
    public int TileWidth = 0;

    [XmlAttribute("tileheight")]
    public int TileHeight = 0;

    [XmlAttribute("tilecount")]
    public int TileCount = 0;

    [XmlAttribute("columns")]
    public int Columns = 0;

    public int Rows (int tileSize)
    {
        return Image.Height / tileSize;
    }

    [XmlElement("image")]
    public Image Image;

    [XmlElement("tile")]
    public Tile[] Tile;
    
    public TileSet ()
    {
    }
}

[XmlRoot("image")]
public partial class Image
{
    [XmlAttribute("source")]
    public string Source = "";

    [XmlAttribute("width")]
    public int Width = 0;

    [XmlAttribute("height")]
    public int Height = 0;

    public Image ()
    {
    }

}
[XmlRoot("tile")]
public partial class Tile
{
    [XmlAttribute("id")]
    public int ID = 0;

    [XmlElement("properties")]
    public Properties Properties;

    public Tile ()
    {
    }
}
[XmlRoot("layer")]
public partial class Layer
{
    [XmlAttribute("name")]
    public string Name = "";

    [XmlAttribute("width")]
    public int Width = 0;

    [XmlAttribute("height")]
    public int Height = 0;

    [XmlElement("data")]
    public Data Data;

    public int[,] GetLevelData ()
    {
        int[,] levelData;
        string[] lines = Data.CSVData.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
        int levelRows = Height;
        int levelColumns = Width;
        levelData = new int[levelRows, levelColumns];
        for ( int currentRow = 0; currentRow < levelRows; currentRow++ )
        {
            string[] integersInLines = lines[currentRow].Split(',', Convert.ToChar(StringSplitOptions.RemoveEmptyEntries));
            for ( int currentColumn = 0; currentColumn < levelColumns; currentColumn++ )
            {
                try
                {
                    levelData[currentRow, currentColumn] = int.Parse(integersInLines[currentColumn]);
                    //Console.Write(levelData[currentRow,currentColumn] + ",");
                }
                catch
                {
                }
            }
            //Console.WriteLine();
        }
        return levelData;
    }

    public Layer ()
    {
    }
}

[XmlRoot ("data")]
public partial class Data
{
    [XmlAttribute("encoding")]
    public string Encoding = "";

    [XmlText]
    public string CSVData = "";

    public Data ()
    {
    }
}

[XmlRoot("properties")]
public partial class Properties
{
    [XmlElement("property")]
    public Property[] Property;

    public Properties ()
    {
    }
}
[XmlRoot("property")]
public partial class Property
{
    [XmlAttribute("name")]
    public string Name = "";
    [XmlAttribute("type")]
    public string Type = "";
    [XmlAttribute("value")]
    public string Value = "";

    public Property ()
    {
    }
}
[XmlRoot ("objectgroup")]
public partial class ObjectGroup
{
    [XmlAttribute("name")]
    public string Name = "";
    [XmlAttribute("visible")]
    public int Visible = 1;
    [XmlElement("object")]
    public ParsedObject[] Object;

    public ObjectGroup ()
    {
    }

}

[XmlRoot("object")]
public partial class ParsedObject : GameObject
{
    [XmlAttribute("id")]
    public int ID = 0;

    [XmlAttribute("gid")]
    public int GID = 0;

    [XmlAttribute("name")]
    public string Name = "";

    [XmlAttribute("type")]
    public string Type = "";

    [XmlAttribute("x")]
    public float X = 0f;

    [XmlAttribute("y")]
    public float Y = 0f;

    [XmlAttribute("width")]
    public float Width = 0f;

    [XmlAttribute("height")]
    public float Height = 0f;

    [XmlAttribute("rotation")]
    public float Rotation = 0f;

    [XmlElement("properties")]
    public Properties Properties;

    [XmlElement("polyline")]
    public Polyline Polyline;

    [XmlElement("polygon")]
    public Polygon Polygon;

    [XmlElement("ellipse")]
    public Ellipse Ellipse;

    public ParsedObject ()
    {
    }


}

[XmlRoot("polyline")]
public partial class Polyline
{
    [XmlAttribute("points")]
    public string Points;

    public Polyline ()
    {
    }
}

[XmlRoot("polygon")]
public partial class Polygon : GameObject
{
    [XmlAttribute("points")]
    public string Points;

    public Polygon ()
    {
    }
}


[XmlRoot("ellipse")]
public partial class Ellipse
{
    public Ellipse ()
    {
    }
}