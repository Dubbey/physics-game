﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Drawing;
public partial class Map
{
    public override string ToString ()
    {
        string layersString = "";
        string tileSetString = "";
        string objectGroupString = "";
        if ( Layer != null )
        {
            for ( int i = 0; i < Layer.Length; i++ )
            {
                layersString += Layer[i].ToString() + Environment.NewLine;
            }
        }

        if ( TileSet != null )
        {
            for ( int i = 0; i < TileSet.Length; i++ )
            {
                tileSetString += TileSet[i].ToString() + Environment.NewLine;
            }
        }

        if ( ObjectGroup != null )
        {
            for ( int i = 0; i < ObjectGroup.Length; i++ )
            {
                objectGroupString += ObjectGroup[i].ToString() + Environment.NewLine;
            }
        }
        return "<?xml version=\"1.0\" encoding=\"UTF - 8\"?>" 
            + Environment.NewLine + "<map version=\"" + Version + "\" orientation=\"" + Orientation + "\" renderorder=\"" + RenderOrder + "\" width=\" height=\"" + Height + "\" tilewidth=\"" + TileWidth + "\" tileheight=\"" + TileHeight + "\" nextobjectid=\"" + NextObjectID + "\">"
            + tileSetString
            + layersString
            + objectGroupString
            + "</map>";
    }
}

public partial class TileSet
{
    public override string ToString ()
    {
        string tileString = "";
        if ( Tile != null )
        {
            for ( int i = 0; i < Tile.Length; i++ )
            {
                tileString += Tile[i].ToString();
            }
        }


        return Environment.NewLine + " <tilset firstgid=\"" + FirstGID + "\" name=\"" + Name + "\" tilewidth=\"" + TileWidth + "\" tileheight=\"" + TileHeight + "\" columns=\"" + Columns + "\">"
            + Environment.NewLine
            + Image.ToString()
            + tileString
            + Environment.NewLine + " </tileset>";
    }
}

public partial class Image
{
    public override string ToString ()
    {
        return "  <image source=\"" + Source + "\" width=\"" + Width + "\" height=\"" + Height + "\"/>";
    }
}

public partial class Tile
{
    public override string ToString ()
    {
        return Environment.NewLine + "  <tile id =\"" + ID + "\">"
            + Environment.NewLine
            + Properties.ToString()
            + Environment.NewLine + "   </properties>"
            + Environment.NewLine + "  </tile>";
    }
}

public partial class Properties
{
    public override string ToString ()
    {
        string propertiesString = "";
        for ( int i = 0; i < Property.Length; i++ )
        {
            propertiesString += Property[i].ToString();
        }

        return "   <properties>"
            + propertiesString;
    }
}

public partial class Property
{
    public override string ToString ()
    {
        return Environment.NewLine + "    <property name=\"" + Name + "\" type=\"" + Type + "\" value=\"" + Value + "\"/>";
    }
}

public partial class Layer
{
    public override string ToString ()
    {
        return Environment.NewLine 
            + " <layer name=\"" + Name + "\" width=\"" + Width + "\" height=\"" + Height + "\">"
            + Environment.NewLine
            + Data.ToString()
            + "  </data>" + Environment.NewLine
            + " </layer>" + Environment.NewLine;
    }
}
public partial class Data
{
    public override string ToString ()
    {

        return "  <data encoding=\"" + Encoding + "\">" + CSVData;
    }
}

public partial class ObjectGroup
{
    public override string ToString ()
    {
        string objectGroupString = "";
        if ( Object != null )
        {
            for ( int i = 0; i < Object.Length; i++ )
            {
                objectGroupString += Object[i].ToString() + Environment.NewLine;
            }
        }

        return " <objectgroup name=\"" + Name + "\" visible=\"" + Visible + "\">"
            + Environment.NewLine
            + objectGroupString
            + " </objectgroup>";
    }
}

public partial class ParsedObject
{
    public override string ToString ()
    {
        string objectString = "";
        if ( GID != 0 )
        {
            objectString = "<object id=\"" + ID + "\" gid=\"" + GID + "\" x=\"" + X + "\" y=\"" + Y + "\" width=\"" + Width + "\" height=\"" + Height + "\"/>";
        }
        else if ( Width != 0 && Height != 0)
        {
            if ( Ellipse != null )
            {
                objectString = "<object id=\"" + ID + "\" x=\"" + X + "\" y=\"" + Y + "\" width=\"" + Width + "\" height=\"" + Height + "\">"
                    + Ellipse.ToString()
                    + Environment.NewLine
                    + "  </object>";
            }
            else
            {
                objectString = "<object id=\"" + ID + "\" x=\"" + X + "\" y=\"" + Y + "\" width=\"" + Width + "\" height=\"" + Height + "\"/>";
            }
        }
        else if (Polyline != null)
        {
            objectString = "<object id=\"" + ID + "\" x=\"" + X + "\" y=\"" + Y + "\">"
                + Polyline.ToString()
                + Environment.NewLine
                + "  </object>";
        }
        else if ( Polygon != null )
        {
            objectString = "<object id=\"" + ID + "\" x=\"" + X + "\" y=\"" + Y + "\">"
                + Polygon.ToString()
                + Environment.NewLine
                + "  </object>";
        }
        else
        {
            objectString = "<object id=\"" + ID + "\" x=\"" + X + "\" y=\"" + Y + "\"/>";
        }

        return "  " + objectString;
    }
}

public partial class Polyline
{
    [XmlIgnore]
    public List<Vec2> vectorPoints;

    public override string ToString ()
    {
        setPoints();
        string pointsToString = "";
        for ( int i = 0; i < vectorPoints.Count; i++ )
        {
            pointsToString += Environment.NewLine + "X,Y = " + vectorPoints[i];
        }
        return Environment.NewLine + "   " + "<polyline points=\"" + Points + "\"/>" + pointsToString;
    }

    private void setPoints ()
    {
        vectorPoints = new List<Vec2>();
        string[] coordinates = Points.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        for ( int i = 0; i < coordinates.Length; i++ )
        {
            string[] splitXY = coordinates[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            float x = float.Parse(splitXY[0]);
            float y = float.Parse(splitXY[1]);
            Vec2 newVector = new Vec2(x,y);
            vectorPoints.Add(newVector);
            //Console.WriteLine(newVector);

        }
    }
}

public partial class Polygon
{
    public override string ToString ()
    {
        List<Vec2> vectorPoints = GetPointsList();
        string pointsToString = "";
        for ( int i = 0; i < vectorPoints.Count; i++ )
        {
            pointsToString += Environment.NewLine + "X,Y = " + vectorPoints[i];
        }
        return Environment.NewLine + "   " + "<polygon points=\"" + Points + "\"/>";
    }

    public List<Vec2> GetPointsList ()
    {
        List<Vec2> vectorPoints = new List<Vec2>();
        string[] coordinates = Points.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        for ( int i = 0; i < coordinates.Length; i++ )
        {
            string[] splitXY = coordinates[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            float x = float.Parse(splitXY[0]);
            float y = float.Parse(splitXY[1]);
            Vec2 newVector = new Vec2(x, y);
            vectorPoints.Add(newVector);
            //Console.WriteLine(newVector);

        }
        return vectorPoints;
    }
    public PointF[] GetPoints ()
    {
        List<PointF> vectorPoints = new List<PointF>();
        Console.WriteLine(vectorPoints.Count);
        string[] coordinates = Points.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        for ( int i = 0; i < coordinates.Length; i++ )
        {
            string[] splitXY = coordinates[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            float x = float.Parse(splitXY[0]);
            float y = float.Parse(splitXY[1]);
            Vec2 newVector = new Vec2(x, y);
            vectorPoints.Add( new PointF(newVector.x, newVector.y) );
            Console.WriteLine(vectorPoints.Count);
        }
        return vectorPoints.ToArray();
    }

}
public partial class Ellipse
{
    public override string ToString ()
    {
        return Environment.NewLine + "   " + "<ellipse/>";
    }
}