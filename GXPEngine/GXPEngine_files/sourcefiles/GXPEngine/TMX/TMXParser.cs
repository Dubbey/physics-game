﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
public class TMXParser
{
    public Map Map
    {
        get; private set;
    }

    public TMXParser ()
    {
    }

    public Map Parse(string filename)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Map));

        TextReader reader = new StreamReader(filename);
        Map = serializer.Deserialize(reader) as Map;
        reader.Close();

        return Map;
    }
}