﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;
public class Vec2 
{
    public static Vec2 zero
    {
        get
        {
            return new Vec2(0,0);
        }
    }

    public float x = 0;
    public float y = 0;

    public Vec2 (float pX = 0, float pY = 0)
    {
        x = pX;
        y = pY;
    }

    public Vec2 Add (Vec2 other)
    {
        x += other.x;
        y += other.y;
        return this;
    }

    public Vec2 Add ( float pX, float pY )
    {
        x += pX;
        y += pY;
        return this;
    }

    public Vec2 Substract (Vec2 other)
    {
        x -= other.x;
        y -= other.y;
        return this;
    }

    public float Length ()
    {
        return Mathf.Sqrt(x * x + y * y);
    }

    public Vec2 Scale (float scalar)
    {
        x *= scalar;
        y *= scalar;

        return this;
    }

    public Vec2 Normalize ()
    {
        float length = Length();
        if ( length != 0 && length != 1 )
        {
            x /= length;
            y /= length;
        }
        return this;
    }

    public Vec2 Clone ()
    {
        return new Vec2(x, y);
    }

    public void SetXY (Vec2 other)
    {
        x = other.x;
        y = other.y;
    }
    public void SetXY ( float pX, float pY)
    {
        x = pX;
        y = pY;
    }
    public float DistanceTo ( Vec2 other )
    {
        float dx = other.x - x;
        float dy = other.y - y;
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    public static float Deg2Rad (float degrees)
    {
        return degrees = (degrees / 180) * Mathf.PI;
    }

    public static float Rad2Deg (float radians)
    {
        return radians = (radians / Mathf.PI) * 180;
    }

    public static Vec2 GetUnitVectorDegrees(float degrees)
    {
        return GetUnitVectorRadians(Deg2Rad(degrees));
    }

    public static Vec2 GetUnitVectorRadians (float radians)
    {
        return new Vec2(Mathf.Cos(radians), Mathf.Sin(radians));
    }

    private static Random _randomDirection = new Random();
    public static Vec2 RandomUnitVector ()
    {
        return GetUnitVectorDegrees(_randomDirection.Next(0, 360));
    }

    public Vec2 SetAngleDegrees (float degrees)
    {
        SetAngleRadians(Deg2Rad(degrees));
        return this;
    }
    public Vec2 SetAngleRadians ( float radians )
    {
        float length = Length();
        x = length * Mathf.Cos(radians);
        y = length * Mathf.Sin(radians);
        return this;
    }
    public float GetAngleRadians ()
    {
        return Mathf.Atan2(y, x);
    }

    public float GetAngleDegrees ()
    {
        return Rad2Deg(GetAngleRadians());
    }
    public Vec2 RotateRadians (float radians)
    {
        float length = Length();
        float cs = Mathf.Cos(radians);
        float sin = Mathf.Sin(radians);

        Vec2 xVec = new Vec2(x * cs, x * sin);
        Vec2 yVec = new Vec2(-y * sin, y * cs);

        return xVec.Add(yVec);
    }

    public Vec2 RotateDegrees (float degrees)
    {
        RotateRadians(Deg2Rad(degrees));
        return this;
    }

    public Vec2 SmoothRotateRadians ( float targetAngleRadians )
    {
        float currentAngle = GetAngleRadians();
        if ( targetAngleRadians > currentAngle )
        {
            currentAngle++;
        }
        else if (targetAngleRadians < currentAngle)
        {
            currentAngle--;
        }
        SetAngleRadians(currentAngle);
        return this;
    }
    public Vec2 SmoothRotateDegrees ( float targetAngleDegrees )
    {
        SmoothRotateRadians(Deg2Rad(targetAngleDegrees));
        return this;
    }

    public Vec2 RotateAroundDegrees ( float degrees )
    {
        RotateAroundRadians(Deg2Rad(degrees));
        return this;
    }

    public Vec2 RotateAroundRadians ( float radians )
    {
        float length = Length();

        float currentAngle = GetAngleRadians();
        float newAngle = currentAngle + radians;
        x = length * Mathf.Cos(newAngle);
        y = length * Mathf.Sin(newAngle);
        return this;
    }


    public override string ToString ()
    {
        return "(" + x + "," + y + ")";
    }
}
