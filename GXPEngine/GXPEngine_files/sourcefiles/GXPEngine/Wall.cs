﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GXPEngine;
using GXPEngine.Core;

public class Wall : AnimationSprite
{
    private Level _level;
    public Wall ( string filename, int cols, int rows, Level pLevel ) : base(filename, cols, rows)
    {
        _level = pLevel;
    }

    public override void Render ( GLContext glContext )
    {
        float levelX = -_level.x;
        if ( x + bounds.x + width > levelX && x - bounds.x < levelX + game.width )
        {
            base.Render(glContext);
        }
    }
}